# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Apr 17 2019)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class WinIPFrame
###########################################################################

class WinIPFrame ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"WinIP", pos = wx.DefaultPosition, size = wx.Size( 325,300 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer1 = wx.BoxSizer( wx.VERTICAL )

		bSizer2 = wx.BoxSizer( wx.VERTICAL )

		self.m_panel1 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer3 = wx.BoxSizer( wx.HORIZONTAL )

		bSizer4 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText1 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Adapter", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText1.Wrap( -1 )

		bSizer4.Add( self.m_staticText1, 1, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 5 )


		bSizer3.Add( bSizer4, 0, wx.ALIGN_CENTER_VERTICAL, 5 )

		bSizer5 = wx.BoxSizer( wx.HORIZONTAL )

		adapt_comboChoices = []
		self.adapt_combo = wx.ComboBox( self.m_panel1, wx.ID_ANY, u"No active adapters", wx.DefaultPosition, wx.DefaultSize, adapt_comboChoices, 0 )
		bSizer5.Add( self.adapt_combo, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )


		bSizer3.Add( bSizer5, 1, wx.EXPAND, 5 )

		self.m_staticline1 = wx.StaticLine( self.m_panel1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
		bSizer3.Add( self.m_staticline1, 0, wx.EXPAND |wx.ALL, 5 )


		self.m_panel1.SetSizer( bSizer3 )
		self.m_panel1.Layout()
		bSizer3.Fit( self.m_panel1 )
		bSizer2.Add( self.m_panel1, 0, wx.EXPAND, 5 )

		self.m_panel2 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer6 = wx.BoxSizer( wx.VERTICAL )

		bSizer8 = wx.BoxSizer( wx.HORIZONTAL )

		self.dhcp_btn = wx.RadioButton( self.m_panel2, wx.ID_ANY, u"DHCP", wx.DefaultPosition, wx.DefaultSize, wx.RB_GROUP )
		self.dhcp_btn.SetValue( True )
		bSizer8.Add( self.dhcp_btn, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.static_btn = wx.RadioButton( self.m_panel2, wx.ID_ANY, u"Static", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer8.Add( self.static_btn, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		fav_comboChoices = []
		self.fav_combo = wx.ComboBox( self.m_panel2, wx.ID_ANY, u"default", wx.DefaultPosition, wx.DefaultSize, fav_comboChoices, 0 )
		bSizer8.Add( self.fav_combo, 1, wx.ALL, 5 )


		bSizer6.Add( bSizer8, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT|wx.EXPAND, 5 )


		self.m_panel2.SetSizer( bSizer6 )
		self.m_panel2.Layout()
		bSizer6.Fit( self.m_panel2 )
		bSizer2.Add( self.m_panel2, 0, wx.EXPAND, 5 )

		self.olv_panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.olv_sizer = wx.BoxSizer( wx.VERTICAL )


		self.olv_panel.SetSizer( self.olv_sizer )
		self.olv_panel.Layout()
		self.olv_sizer.Fit( self.olv_panel )
		bSizer2.Add( self.olv_panel, 1, wx.EXPAND, 5 )


		bSizer1.Add( bSizer2, 0, wx.EXPAND, 5 )


		self.SetSizer( bSizer1 )
		self.Layout()
		self.m_menubar1 = wx.MenuBar( 0 )
		self.File = wx.Menu()
		self.menu_exit = wx.MenuItem( self.File, wx.ID_ANY, u"Exit", wx.EmptyString, wx.ITEM_NORMAL )
		self.File.Append( self.menu_exit )

		self.m_menubar1.Append( self.File, u"File" )

		self.fav_menu = wx.Menu()
		self.add_fav = wx.MenuItem( self.fav_menu, wx.ID_ANY, u"New", wx.EmptyString, wx.ITEM_NORMAL )
		self.fav_menu.Append( self.add_fav )

		self.m_menubar1.Append( self.fav_menu, u"Favorites" )

		self.SetMenuBar( self.m_menubar1 )


		self.Centre( wx.BOTH )

		# Connect Events
		self.adapt_combo.Bind( wx.EVT_COMBOBOX, self.on_select_adapter )
		self.dhcp_btn.Bind( wx.EVT_RADIOBUTTON, self.on_radio )
		self.static_btn.Bind( wx.EVT_RADIOBUTTON, self.on_radio )
		self.fav_combo.Bind( wx.EVT_COMBOBOX, self.on_select_fav )
		self.Bind( wx.EVT_MENU, self.on_exit, id = self.menu_exit.GetId() )
		self.Bind( wx.EVT_MENU, self.on_fav_new, id = self.add_fav.GetId() )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def on_select_adapter( self, event ):
		event.Skip()

	def on_radio( self, event ):
		event.Skip()


	def on_select_fav( self, event ):
		event.Skip()

	def on_exit( self, event ):
		event.Skip()

	def on_fav_new( self, event ):
		event.Skip()


###########################################################################
## Class Favorites
###########################################################################

class Favorites ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Favorite", pos = wx.DefaultPosition, size = wx.Size( 330,-1 ), style = wx.DEFAULT_DIALOG_STYLE|wx.RESIZE_BORDER )

		self.SetSizeHints( wx.Size( -1,400 ), wx.DefaultSize )

		bSizer10 = wx.BoxSizer( wx.VERTICAL )

		bSizer11 = wx.BoxSizer( wx.VERTICAL )

		self.m_panel4 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer12 = wx.BoxSizer( wx.VERTICAL )

		bSizer15 = wx.BoxSizer( wx.HORIZONTAL )

		bSizer22 = wx.BoxSizer( wx.VERTICAL )

		self.m_staticText3 = wx.StaticText( self.m_panel4, wx.ID_ANY, u"Stored IP's", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText3.Wrap( -1 )

		bSizer22.Add( self.m_staticText3, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_CENTER_HORIZONTAL, 5 )


		bSizer15.Add( bSizer22, 1, wx.ALIGN_CENTER_VERTICAL, 5 )

		bSizer23 = wx.BoxSizer( wx.VERTICAL )


		bSizer15.Add( bSizer23, 0, wx.EXPAND, 5 )


		bSizer12.Add( bSizer15, 0, wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 5 )

		self.static_olv_panel = wx.Panel( self.m_panel4, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.static_olv_sizer = wx.BoxSizer( wx.VERTICAL )


		self.static_olv_panel.SetSizer( self.static_olv_sizer )
		self.static_olv_panel.Layout()
		self.static_olv_sizer.Fit( self.static_olv_panel )
		bSizer12.Add( self.static_olv_panel, 1, wx.EXPAND |wx.ALL, 5 )

		bSizer19 = wx.BoxSizer( wx.VERTICAL )

		sbSizer2 = wx.StaticBoxSizer( wx.StaticBox( self.m_panel4, wx.ID_ANY, u"Gateway" ), wx.VERTICAL )

		self.static_gateway_txt = wx.TextCtrl( sbSizer2.GetStaticBox(), wx.ID_ANY, u"None", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer2.Add( self.static_gateway_txt, 0, wx.ALL|wx.EXPAND, 5 )


		bSizer19.Add( sbSizer2, 0, wx.EXPAND, 5 )


		bSizer12.Add( bSizer19, 0, wx.EXPAND, 5 )

		bSizer16 = wx.BoxSizer( wx.VERTICAL )

		bSizer17 = wx.BoxSizer( wx.HORIZONTAL )

		self.add_ip_btn = wx.Button( self.m_panel4, wx.ID_ANY, u"Add IP", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer17.Add( self.add_ip_btn, 0, wx.ALL, 5 )

		self.save_btn = wx.Button( self.m_panel4, wx.ID_ANY, u"Save", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer17.Add( self.save_btn, 0, wx.ALL, 5 )

		self.del_btn = wx.Button( self.m_panel4, wx.ID_ANY, u"Delete Favorite", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer17.Add( self.del_btn, 0, wx.ALL, 5 )


		bSizer16.Add( bSizer17, 0, wx.ALIGN_CENTER_HORIZONTAL, 5 )

		bSizer25 = wx.BoxSizer( wx.HORIZONTAL )


		bSizer16.Add( bSizer25, 1, wx.ALIGN_CENTER_HORIZONTAL, 5 )


		bSizer12.Add( bSizer16, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.EXPAND, 5 )


		self.m_panel4.SetSizer( bSizer12 )
		self.m_panel4.Layout()
		bSizer12.Fit( self.m_panel4 )
		bSizer11.Add( self.m_panel4, 1, wx.EXPAND |wx.ALL, 5 )


		bSizer10.Add( bSizer11, 1, wx.EXPAND, 5 )


		self.SetSizer( bSizer10 )
		self.Layout()
		self.rc_menu = wx.Menu()
		self.rc_menu_add = wx.MenuItem( self.rc_menu, wx.ID_ANY, u"Add IP", wx.EmptyString, wx.ITEM_NORMAL )
		self.rc_menu.Append( self.rc_menu_add )

		self.rc_menu_delete = wx.MenuItem( self.rc_menu, wx.ID_ANY, u"Delete IP", wx.EmptyString, wx.ITEM_NORMAL )
		self.rc_menu.Append( self.rc_menu_delete )

		self.Bind( wx.EVT_RIGHT_DOWN, self.FavoritesOnContextMenu )


		self.Centre( wx.BOTH )

		# Connect Events
		self.add_ip_btn.Bind( wx.EVT_BUTTON, self.on_add_ip )
		self.save_btn.Bind( wx.EVT_BUTTON, self.on_save )
		self.del_btn.Bind( wx.EVT_BUTTON, self.on_delete_favorite )
		self.Bind( wx.EVT_MENU, self.on_add_ip, id = self.rc_menu_add.GetId() )
		self.Bind( wx.EVT_MENU, self.on_delete_ip, id = self.rc_menu_delete.GetId() )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def on_add_ip( self, event ):
		event.Skip()

	def on_save( self, event ):
		event.Skip()

	def on_delete_favorite( self, event ):
		event.Skip()


	def on_delete_ip( self, event ):
		event.Skip()

	def FavoritesOnContextMenu( self, event ):
		self.PopupMenu( self.rc_menu, event.GetPoint() )


###########################################################################
## Class AddIP
###########################################################################

class AddIP ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Add IP", pos = wx.DefaultPosition, size = wx.DefaultSize, style = wx.DEFAULT_DIALOG_STYLE )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer20 = wx.BoxSizer( wx.VERTICAL )

		bSizer21 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText4 = wx.StaticText( self, wx.ID_ANY, u"IP Address", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText4.Wrap( -1 )

		bSizer21.Add( self.m_staticText4, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.ipaddress_txt = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.ipaddress_txt.SetMinSize( wx.Size( 200,-1 ) )

		bSizer21.Add( self.ipaddress_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer20.Add( bSizer21, 0, wx.EXPAND|wx.ALL, 5 )

		bSizer22 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText5 = wx.StaticText( self, wx.ID_ANY, u"Subnet", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText5.Wrap( -1 )

		bSizer22.Add( self.m_staticText5, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.subnet_txt = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.subnet_txt.SetMinSize( wx.Size( 200,-1 ) )

		bSizer22.Add( self.subnet_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer20.Add( bSizer22, 1, wx.EXPAND|wx.ALL, 5 )

		bSizer23 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_button6 = wx.Button( self, wx.ID_ANY, u"Add", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer23.Add( self.m_button6, 0, wx.ALL, 5 )

		self.m_button7 = wx.Button( self, wx.ID_ANY, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer23.Add( self.m_button7, 0, wx.ALL, 5 )


		bSizer20.Add( bSizer23, 0, wx.ALIGN_RIGHT, 5 )


		self.SetSizer( bSizer20 )
		self.Layout()
		bSizer20.Fit( self )

		self.Centre( wx.BOTH )

		# Connect Events
		self.m_button6.Bind( wx.EVT_BUTTON, self.on_add )
		self.m_button7.Bind( wx.EVT_BUTTON, self.on_cancel )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def on_add( self, event ):
		event.Skip()

	def on_cancel( self, event ):
		event.Skip()


