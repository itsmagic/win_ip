# -*- mode: python -*-

block_cipher = None

project_name = 'win_ip'
project_version = 'v2.0.2'
project_icon = 'win_ico.ico'
single_file = False

## Do not modify below this line.

a = Analysis([f'{project_name}.py'],
             pathex=[f'C:\\Users\\Jim Maciejewski\\Documents\\{project_name}'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)

##### include mydir in distribution #######
def extra_datas(icon):
    def rec_glob(p, files):
        import os
        import glob
        for d in glob.glob(p):
            if os.path.isfile(d):
                files.append(d)
            rec_glob("%s/*" % d, files)
    files = []
    rec_glob("%s/*" % icon, files)
    extra_datas = []
    for f in files:
        extra_datas.append((f, f, 'icon'))

    return extra_datas
############################################
a.datas += extra_datas('icon')
a.datas += extra_datas('doc')


pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name=f'{project_name}_{project_version}.exe',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=False,
          icon=f'icon\\{project_icon}')
          #uac_admin=True )

if not single_file:
    coll = COLLECT(exe,
                   a.binaries,
                   a.zipfiles,
                   a.datas,
                   strip=False,
                   upx=True,
                   upx_exclude=[],
                   name=project_name)

