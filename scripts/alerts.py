from pydispatch import dispatcher
from threading import Thread


class AlertJobs(Thread):

    def __init__(self, queue):
        Thread.__init__(self)
        self.queue = queue

    def run(self):
        while True:
            # gets the job from the queue
            job = self.queue.get()
            self.send_alert(job)
            # send a signal to the queue that the job is done
            self.queue.task_done()

    def send_alert(self, job):
        dispatcher.send(signal="Alert", sender=self, caption=job[1], message=job[0])
