import win_ip_gui
import ipaddress


class AddIPDialog(win_ip_gui.AddIP):
    """Sets the preferences """

    def __init__(self, parent, adapter):
        win_ip_gui.AddIP.__init__(self, parent)
        self.parent = parent
        self.adapter = adapter

    def on_next_field(self, event):
        # print('in on next field')
        self.subnet_txt.SetFocus()

    def on_add(self, event):
        new_ip = self.ip_address_txt.GetValue()
        new_subnet = self.subnet_txt.GetValue()
        if not self.parent.verify_ip_valid(ip_address=new_ip, subnet=new_subnet):
            return
        self.adapter.ip_addresses.append(str(ipaddress.ip_address(new_ip)))
        self.adapter.subnet_addresses.append(new_subnet)
        self.parent.save_config(preferences=self.parent.preferences)
        self.parent.set_static()
        self.Destroy()

    def create_ip_line(self, ip, subnet):
        line = self.parent.create_line()
        line.ip_address = ip
        line.subnet = subnet
        return line

    def check_if_ipv4(self, ip):
        my_ip = ipaddress.ip_address(ip)
        if my_ip.version == 4:
            return True
        else:
            return False

    def on_cancel(self, event):
        self.Destroy()
