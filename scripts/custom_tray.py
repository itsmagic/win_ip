import wx
import wx.adv
import os
import sys


class CustomTaskBarIcon(wx.adv.TaskBarIcon):
    """Shows a icon in the task bar"""

    def __init__(self, frame):
        """Constructor"""
        wx.adv.TaskBarIcon.__init__(self)
        self.frame = frame
        icon_bundle = wx.IconBundle()
        icon_bundle.AddIcon(self.resource_path(os.path.join("icon", "wi_unknown.ico")), wx.BITMAP_TYPE_ANY)
        self.SetIcon(icon_bundle.GetIconOfExactSize(wx.Size(24, 24)))
        self.Bind(wx.adv.EVT_TASKBAR_LEFT_DOWN, self.OnTaskBarLeftClick)
        self.Bind(wx.adv.EVT_TASKBAR_RIGHT_DOWN, self.OnTaskBarRightClick)

    def resource_path(self, relative_path):
        """ Get absolute path to resource, works for dev and for PyInstaller """
        try:
            # PyInstaller creates a temp folder and stores path in _MEIPASS
            base_path = sys._MEIPASS
        except Exception:
            base_path = os.path.abspath(".")

        return os.path.join(base_path, relative_path)

    def CreatePopupMenu(self, evt=None):
        """
        This method is called by the base class when it needs to popup
        the menu for the default EVT_RIGHT_DOWN event.  Just create
        the menu how you want it and return it from this function,
        the base class takes care of the rest.
        """
        self.rc_menu = wx.Menu()
        # menu.Append(wx.ID_ANY, "Open Program")
        # self.rc_menu.Append(wx.ID_ANY, "Restore")

        self.adapter_rc_menu = wx.MenuItem(self.rc_menu, wx.ID_ANY, self.frame.adapt_cmb.GetValue(), wx.EmptyString, wx.ITEM_NORMAL)
        self.rc_menu.Append(self.adapter_rc_menu)
        self.adapter_rc_menu.Enable(False)
        if self.frame.static_btn.GetValue():
            self.dhcp_rc_menu = wx.MenuItem(self.rc_menu, wx.ID_ANY, u"Set DHCP", wx.EmptyString, wx.ITEM_NORMAL)
            self.rc_menu.Append(self.dhcp_rc_menu)
            self.Bind(wx.EVT_MENU, self.set_dhcp, id=self.dhcp_rc_menu.GetId())
        if self.frame.dhcp_btn.GetValue():
            self.static_rc_menu = wx.MenuItem(self.rc_menu, wx.ID_ANY, u"Set Static", wx.EmptyString, wx.ITEM_NORMAL)
            self.rc_menu.Append(self.static_rc_menu)
            self.Bind(wx.EVT_MENU, self.set_static, id=self.static_rc_menu.GetId())
        self.restore_rc_menu = wx.MenuItem(self.rc_menu, wx.ID_ANY, u"Show Window", wx.EmptyString, wx.ITEM_NORMAL)
        self.rc_menu.Append(self.restore_rc_menu)
        self.rc_menu.AppendSeparator()
        self.Bind(wx.EVT_MENU, self.OnTaskBarLeftClick, id=self.restore_rc_menu.GetId())
        self.exit_rc_menu = wx.MenuItem(self.rc_menu, wx.ID_ANY, u"Exit Program", wx.EmptyString, wx.ITEM_NORMAL)
        self.rc_menu.Append(self.exit_rc_menu)
        self.Bind(wx.EVT_MENU, self.OnExit, id=self.exit_rc_menu.GetId())
        return self.rc_menu

    def set_dhcp(self, event):
        self.frame.set_dhcp()

    def set_static(self, event):
        self.frame.set_static()

    def OnExit(self, evt):
        """Exit the program"""
        self.frame.onClose(evt)

    def OnTaskBarActivate(self, evt):
        """"""
        pass

    def OnTaskBarClose(self, evt):
        """
        Destroy the taskbar icon and frame from the taskbar icon itself
        """
        self.frame.Close()

    def OnTaskBarLeftClick(self, evt):
        """
        Create the right-click menu
        """
        # self.frame.toggle_show(evt)
        self.frame.bring_to_top(evt)

    def OnTaskBarRightClick(self, evt):
        """ Handle a right click """
        menu = self.CreatePopupMenu()
        self.PopupMenu(menu)
        menu.Destroy()

    def on_dhcp(self):
        """ Sets the dhcp icon """
        icon_bundle = wx.IconBundle()
        icon_bundle.AddIcon(self.resource_path(os.path.join("icon", "wi_dhcp.ico")), wx.BITMAP_TYPE_ANY)
        self.SetIcon(icon_bundle.GetIconOfExactSize(wx.Size(24, 24)))

    def on_static(self):
        """ Sets the static icon """

        icon_bundle = wx.IconBundle()
        icon_bundle.AddIcon(self.resource_path(os.path.join("icon", "wi_static.ico")), wx.BITMAP_TYPE_ANY)
        self.SetIcon(icon_bundle.GetIconOfExactSize(wx.Size(24, 24)))

    def on_unknown(self):
        """ Sets the unknown icon """

        icon_bundle = wx.IconBundle()
        icon_bundle.AddIcon(self.resource_path(os.path.join("icon", "wi_unknown.ico")), wx.BITMAP_TYPE_ANY)
        self.SetIcon(icon_bundle.GetIconOfExactSize(wx.Size(24, 24)))
