from threading import Thread
import time
import wmi
import pythoncom
import queue
import ipaddress
from serial.tools.list_ports_windows import comports
from pydispatch import dispatcher
from dataclasses import dataclass, field


@dataclass
class InterfaceConfig:
    index: int = 0
    description: str = ''
    ip_addresses: list = field(default_factory=list)
    subnet_addresses: list = field(default_factory=list)
    gateway_addresses: list = field(default_factory=list)
    dhcp_enabled: bool = True


@dataclass
class SerialPortConfig:
    # {'device': 'COM3',
    #  'description': 'USB Serial Device (COM3)',
    #  'hwid': 'USB VID:PID=8087:0ACA SER=7'}
    device: str = ''
    description: str = ''
    hwid: str = ''


class IPMonitorControl(Thread):
    """The ip monitor thread"""

    def __init__(self, my_queue):
        """Init Worker Thread Class."""
        self.shutdown = False
        self.my_queue = my_queue
        self.interface_list = []
        self.serial_port_list = []
        self.first_serial_run = True
        Thread.__init__(self)

    def run(self):
        """Run Worker Thread."""
        pythoncom.CoInitialize()
        self.interface_list = self.get_current_interfaces()
        while True:
            # gets the job from the queue
            job = self.my_queue.get()
            # print 'job: ', job
            getattr(self, job[0])(job)
            # send a signal to the queue that the job is done
            self.my_queue.task_done()

    def check_serial(self, command):
        # Get a list of serial ports and see if there have been any changes
        current_ports = self.get_serial_ports()
        if self.first_serial_run:
            self.first_serial_run = False
            self.serial_port_list = current_ports
            return
        if self.serial_port_list != current_ports:
            if len(self.serial_port_list) > len(current_ports):
                # print('port lost')
                pass
            else:
                # print('port found')
                new_ports = []
                for item in current_ports:
                    if item not in self.serial_port_list:
                        new_ports.append(item)
                self.send_serial_update(found_ports=new_ports)
        self.serial_port_list = current_ports

    def get_serial_ports(self):
        serial_ports = []
        for item in comports():
            serial_ports.append(SerialPortConfig(device=item.device,
                                                 description=item.description,
                                                 hwid=item.hwid))
        return serial_ports

    def check_changes(self, command):
        # check if configs have changed (polling)
        # only send a update if necessary
        current_interfaces = self.get_current_interfaces()

        if self.interface_list != current_interfaces:
            self.interface_list = current_interfaces
            self.send_update()
        # else:
        #     print('no change')

    def get_current_interfaces(self):
        """Get the nic's"""
        interfaces = []
        for interface in wmi.WMI().Win32_NetworkAdapterConfiguration(IPEnabled=True):
            interfaces.append(InterfaceConfig(index=interface.Index,
                                              description=interface.Description,
                                              ip_addresses=list(interface.IPAddress),
                                              subnet_addresses=list(interface.IPSubnet),
                                              gateway_addresses=interface.DefaultIPGateway,
                                              dhcp_enabled=interface.DHCPEnabled))

        return interfaces

    def match_interface(self, interface):
        for item in wmi.WMI().Win32_NetworkAdapterConfiguration(IPEnabled=True):
            if interface.index == item.Index:
                return item

        return None

    def set_dhcp(self, job):
        interface = job[1]
        my_interface = self.match_interface(interface)
        if my_interface is not None:
            success = my_interface.EnableDHCP()
            if success != (0,):
                self.send_error("Setting DHCP failed with error: " + str(success))
            else:
                dispatcher.send(signal="Set Done")

    def set_static(self, job):
        # print('in set static: ', job)
        adapter = job[1]

        my_interface = self.match_interface(adapter)
        if my_interface is not None:
            # print('setting static: ', adapter.ip_addresses, adapter.subnet_addresses)
            ip_v4_addresses = []
            subnet_v4_addresses = []
            for i, item in enumerate(adapter.ip_addresses):
                if ipaddress.ip_address(item).version == 4:
                    ip_v4_addresses.append(item)
                    subnet_v4_addresses.append(adapter.subnet_addresses[i])

            success = my_interface.EnableStatic(IPAddress=ip_v4_addresses, SubnetMask=subnet_v4_addresses)
            if success != (0,):
                self.send_error("Setting Static failed with error: " + str(success))
            else:
                dispatcher.send(signal="Set Done")

    def send_update(self, job=None):
        # print('sending update')
        dispatcher.send(signal="Update", sender=self, interface_list=self.interface_list)

    def send_serial_update(self, job=None, found_ports=None):
        # print('sending update')
        dispatcher.send(signal="Serial Update", sender=self, serial_list=found_ports)

    def send_error(self, info):
        dispatcher.send(signal="Alert", sender=self, caption='Error', message=info)


def incoming(sender, interface_list):
    print('in update')
    print(interface_list)


def main():
    """Launch the main program"""
    dispatcher.connect(incoming,
                       signal="IP Monitor",
                       sender=dispatcher.Any)
    dispatcher.connect(incoming,
                       signal="Update",
                       sender=dispatcher.Any)
    control_queue = queue.Queue()
    my_monitor = IPMonitorControl(control_queue)
    my_monitor.setDaemon(True)
    my_monitor.start()

    count = 0
    while True:
        count += 1
        control_queue.put(['check_changes'])
        if count >= 2:
            break
        time.sleep(5)


if __name__ == '__main__':
    main()
