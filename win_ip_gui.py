# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Apr 17 2019)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class WinIPFrame
###########################################################################

class WinIPFrame ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( -1,-1 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.Size( 350,300 ), wx.DefaultSize )

		bSizer1 = wx.BoxSizer( wx.VERTICAL )

		self.m_panel1 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer2 = wx.BoxSizer( wx.VERTICAL )

		bSizer3 = wx.BoxSizer( wx.VERTICAL )

		bSizer4 = wx.BoxSizer( wx.VERTICAL )

		adapt_cmbChoices = []
		self.adapt_cmb = wx.ComboBox( self.m_panel1, wx.ID_ANY, u"No active adapters", wx.DefaultPosition, wx.DefaultSize, adapt_cmbChoices, wx.CB_READONLY )
		bSizer4.Add( self.adapt_cmb, 1, wx.ALL|wx.ALIGN_RIGHT|wx.EXPAND, 5 )


		bSizer3.Add( bSizer4, 0, wx.EXPAND, 5 )

		bSizer5 = wx.BoxSizer( wx.HORIZONTAL )

		self.dhcp_btn = wx.RadioButton( self.m_panel1, wx.ID_ANY, u"DHCP", wx.DefaultPosition, wx.DefaultSize, wx.RB_GROUP )
		self.dhcp_btn.SetValue( True )
		bSizer5.Add( self.dhcp_btn, 0, wx.ALL, 5 )

		self.static_btn = wx.RadioButton( self.m_panel1, wx.ID_ANY, u"Static", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer5.Add( self.static_btn, 0, wx.ALL, 5 )


		bSizer3.Add( bSizer5, 0, wx.ALIGN_RIGHT, 5 )


		bSizer2.Add( bSizer3, 0, wx.EXPAND, 5 )

		self.olv_panel = wx.Panel( self.m_panel1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.olv_sizer = wx.BoxSizer( wx.VERTICAL )


		self.olv_panel.SetSizer( self.olv_sizer )
		self.olv_panel.Layout()
		self.olv_sizer.Fit( self.olv_panel )
		self.rc_menu = wx.Menu()
		self.rc_on_add = wx.MenuItem( self.rc_menu, wx.ID_ANY, u"Add", wx.EmptyString, wx.ITEM_NORMAL )
		self.rc_menu.Append( self.rc_on_add )

		self.rc_on_del = wx.MenuItem( self.rc_menu, wx.ID_ANY, u"Delete", wx.EmptyString, wx.ITEM_NORMAL )
		self.rc_menu.Append( self.rc_on_del )

		self.olv_panel.Bind( wx.EVT_RIGHT_DOWN, self.olv_panelOnContextMenu )

		bSizer2.Add( self.olv_panel, 1, wx.EXPAND, 5 )


		self.m_panel1.SetSizer( bSizer2 )
		self.m_panel1.Layout()
		bSizer2.Fit( self.m_panel1 )
		bSizer1.Add( self.m_panel1, 1, wx.EXPAND, 5 )


		self.SetSizer( bSizer1 )
		self.Layout()
		bSizer1.Fit( self )
		self.m_menubar1 = wx.MenuBar( 0 )
		self.m_menu1 = wx.Menu()
		self.m_menuItem2 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Clear All Stored Settings", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu1.Append( self.m_menuItem2 )

		self.updates_chk = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Check for updates", wx.EmptyString, wx.ITEM_CHECK )
		self.m_menu1.Append( self.updates_chk )
		self.updates_chk.Check( True )

		self.m_menuItem1 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Exit", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu1.Append( self.m_menuItem1 )

		self.m_menubar1.Append( self.m_menu1, u"File" )

		self.m_menu3 = wx.Menu()
		self.monitor_chk = wx.MenuItem( self.m_menu3, wx.ID_ANY, u"Monitor Serial Ports", wx.EmptyString, wx.ITEM_CHECK )
		self.m_menu3.Append( self.monitor_chk )
		self.monitor_chk.Check( True )

		self.m_menuItem7 = wx.MenuItem( self.m_menu3, wx.ID_ANY, u"Show Last Serial Ports", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu3.Append( self.m_menuItem7 )

		self.m_menubar1.Append( self.m_menu3, u"Serial" )

		self.SetMenuBar( self.m_menubar1 )


		self.Centre( wx.BOTH )

		# Connect Events
		self.adapt_cmb.Bind( wx.EVT_COMBOBOX, self.on_select_adapter )
		self.dhcp_btn.Bind( wx.EVT_RADIOBUTTON, self.on_dhcp_radio )
		self.static_btn.Bind( wx.EVT_RADIOBUTTON, self.on_static_radio )
		self.Bind( wx.EVT_MENU, self.on_add, id = self.rc_on_add.GetId() )
		self.Bind( wx.EVT_MENU, self.on_delete, id = self.rc_on_del.GetId() )
		self.Bind( wx.EVT_MENU, self.on_clear_settings, id = self.m_menuItem2.GetId() )
		self.Bind( wx.EVT_MENU, self.on_check_for_updates, id = self.updates_chk.GetId() )
		self.Bind( wx.EVT_MENU, self.on_exit, id = self.m_menuItem1.GetId() )
		self.Bind( wx.EVT_MENU, self.on_monitor_serial, id = self.monitor_chk.GetId() )
		self.Bind( wx.EVT_MENU, self.on_show_last_serial_ports, id = self.m_menuItem7.GetId() )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def on_select_adapter( self, event ):
		event.Skip()

	def on_dhcp_radio( self, event ):
		event.Skip()

	def on_static_radio( self, event ):
		event.Skip()

	def on_add( self, event ):
		event.Skip()

	def on_delete( self, event ):
		event.Skip()

	def on_clear_settings( self, event ):
		event.Skip()

	def on_check_for_updates( self, event ):
		event.Skip()

	def on_exit( self, event ):
		event.Skip()

	def on_monitor_serial( self, event ):
		event.Skip()

	def on_show_last_serial_ports( self, event ):
		event.Skip()

	def olv_panelOnContextMenu( self, event ):
		self.olv_panel.PopupMenu( self.rc_menu, event.GetPoint() )


###########################################################################
## Class AddIP
###########################################################################

class AddIP ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Add an IP to adapter", pos = wx.DefaultPosition, size = wx.DefaultSize, style = wx.DEFAULT_DIALOG_STYLE )

		self.SetSizeHints( wx.Size( 300,-1 ), wx.DefaultSize )

		bSizer8 = wx.BoxSizer( wx.VERTICAL )

		bSizer9 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText2 = wx.StaticText( self, wx.ID_ANY, u"IP Address", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText2.Wrap( -1 )

		self.m_staticText2.SetMinSize( wx.Size( 80,-1 ) )

		bSizer9.Add( self.m_staticText2, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.ip_address_txt = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_ENTER )
		bSizer9.Add( self.ip_address_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer8.Add( bSizer9, 0, wx.EXPAND, 5 )

		bSizer10 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText3 = wx.StaticText( self, wx.ID_ANY, u"Subnet", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText3.Wrap( -1 )

		self.m_staticText3.SetMinSize( wx.Size( 80,-1 ) )

		bSizer10.Add( self.m_staticText3, 0, wx.ALL, 5 )

		self.subnet_txt = wx.TextCtrl( self, wx.ID_ANY, u"255.255.255.0", wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_ENTER )
		bSizer10.Add( self.subnet_txt, 1, wx.ALL, 5 )


		bSizer8.Add( bSizer10, 0, wx.EXPAND, 5 )

		bSizer11 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_button1 = wx.Button( self, wx.ID_ANY, u"Add", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer11.Add( self.m_button1, 0, wx.ALL, 5 )

		self.m_button2 = wx.Button( self, wx.ID_ANY, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer11.Add( self.m_button2, 0, wx.ALL, 5 )


		bSizer8.Add( bSizer11, 0, wx.ALIGN_RIGHT, 5 )


		self.SetSizer( bSizer8 )
		self.Layout()
		bSizer8.Fit( self )

		self.Centre( wx.BOTH )

		# Connect Events
		self.ip_address_txt.Bind( wx.EVT_TEXT_ENTER, self.on_next_field )
		self.subnet_txt.Bind( wx.EVT_TEXT_ENTER, self.on_add )
		self.m_button1.Bind( wx.EVT_BUTTON, self.on_add )
		self.m_button2.Bind( wx.EVT_BUTTON, self.on_cancel )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def on_next_field( self, event ):
		event.Skip()

	def on_add( self, event ):
		event.Skip()


	def on_cancel( self, event ):
		event.Skip()


