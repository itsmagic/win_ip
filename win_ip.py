import wx
import os
import sys
import webbrowser
import pickle
import queue
import win_ip_gui
import ipaddress
from win32com.shell import shell
from ObjectListView import ObjectListView, ColumnDefn
from pydispatch import dispatcher
from scripts import auto_update, adapter_prefs, alerts, custom_tray, win_ip_control, admin
from dataclasses import dataclass, field


@dataclass
class IPLine:
    """An IP Config for our list"""
    ip_address: str = ''
    subnet: str = ''


@dataclass
class Adapter:
    """Addresses stored"""
    index: int = 0
    ip_addresses: list = field(default_factory=list)
    subnet_addresses: list = field(default_factory=list)
    gateway_addresses: list = field(default_factory=list)


@dataclass
class Preferences:
    adapter_prefs: list = field(default_factory=list)
    check_for_updates: bool = True
    monitor_serial: bool = True
    last_serial_ports: list = field(default_factory=list)


class Win_IP_Frame(win_ip_gui.WinIPFrame):
    def __init__(self, parent):
        win_ip_gui.WinIPFrame.__init__(self, parent)

        icon_bundle = wx.IconBundle()
        icon_bundle.AddIcon(self.resource_path(os.path.join("icon", "win_ico.ico")), wx.BITMAP_TYPE_ANY)
        self.SetIcons(icon_bundle)
        self.name = "Win IP"
        self.version = "v2.0.2"
        self.doc_path = os.path.expanduser(os.path.join(os.path.expandvars(r'%APPDATA%'), self.name))
        self.storage_file = os.path.join(self.doc_path, "_".join(self.name.split()) + ".pkl")
        self.SetTitle(f'{self.name} {self.version}')

        self.preferences = self.load_config()

        self.ip_list = ObjectListView(self.olv_panel, wx.ID_ANY,
                                      style=wx.LC_REPORT | wx.SUNKEN_BORDER)
        self.ip_list.SetColumns([ColumnDefn("IP Address", "left", 180, "ip_address", valueSetter=self.cell_update_ip),
                                 ColumnDefn("Subnet Mask", "left", 120, "subnet", valueSetter=self.cell_update_subnet)])
        self.ip_list.cellEditMode = ObjectListView.CELLEDIT_DOUBLECLICK
        # self.ip_list.Bind(EVT_CELL_EDIT_FINISHING, self.check_edited_cell)
        self.ip_list.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self.on_right_click)
        self.olv_sizer.Add(self.ip_list, 1, wx.EXPAND, 0)
        self.updates_chk.Check(self.preferences.check_for_updates)
        dispatcher.connect(self.update_required,
                           signal="Software Update",
                           sender=dispatcher.Any)
        if self.preferences.check_for_updates:
            self.do_update_check()

        self.control_queue = queue.Queue()
        self.monitor = win_ip_control.IPMonitorControl(self.control_queue)
        self.monitor.setDaemon(True)
        self.monitor.start()
        self.alert_queue = queue.Queue()
        self.alert_thread = alerts.AlertJobs(self.alert_queue)
        self.alert_thread.setDaemon(True)
        self.alert_thread.start()

        dispatcher.connect(self.update,
                           signal="Update",
                           sender=dispatcher.Any)
        dispatcher.connect(self.serial_update,
                           signal="Serial Update",
                           sender=dispatcher.Any)
        dispatcher.connect(self.alert,
                           signal="Alert",
                           sender=dispatcher.Any)
        dispatcher.connect(self.done,
                           signal="Set Done",
                           sender=dispatcher.Any)

        self.interface_list = []
        self.selected = None
        self.tray_icon = custom_tray.CustomTaskBarIcon(self)
        self.Bind(wx.EVT_ICONIZE, self.onMinimize)
        self.Bind(wx.EVT_CLOSE, self.onMinimize)

        self.control_queue.put(['send_update'])
        self.redraw_timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.on_refresh, self.redraw_timer)
        self.redraw_timer.Start(1000)
        # self.Show()
        if not shell.IsUserAnAdmin():
            message = ("I\'m not running as an administrator.\r " +
                       "I kinda need to be an administrator.\r " +
                       "I probably won\'t have much luck changing\r " +
                       "network interfaces unless I\'m an administrator.")
            self.alert_queue.put([message, 'Not an administrator'])

    def resource_path(self, relative_path):
        """ Get absolute path to resource, works for dev and for PyInstaller """
        try:
            # PyInstaller creates a temp folder and stores path in _MEIPASS
            base_path = sys._MEIPASS
        except Exception:
            base_path = os.path.abspath(".")

        return os.path.join(base_path, relative_path)

    def on_monitor_serial(self, event):
        self.preferences.monitor_serial = self.monitor_chk.IsChecked()
        self.save_config(preferences=self.preferences)

    def on_show_last_serial_ports(self, event):
        self.serial_update(sender=None, serial_list=self.preferences.last_serial_ports)

    def do_update_check(self):
        update_thread = auto_update.AutoUpdate(server_url="https://magicsoftware.ornear.com", program_name=self.name, program_version=self.version)
        update_thread.setDaemon(True)
        update_thread.start()

    def update_required(self, sender, message, url):
        """Show the update page"""
        dlg = wx.MessageDialog(parent=self,
                               message='An update is available. \rWould you like to go to the download page?',
                               caption='Update available',
                               style=wx.OK | wx.CANCEL)

        if dlg.ShowModal() == wx.ID_OK:
            webbrowser.open(url)

    def done(self, sender):
        self.dhcp_btn.Enable(True)
        self.static_btn.Enable(True)

    def check_if_duplicate(self, ip):
        for interface in self.interface_list:
            for address in interface.ip_addresses:
                if address == ip:
                    return interface
        return

    def verify_ip_valid(self, ip_address, subnet, subnet_change=False):
        try:
            ipaddress.IPv4Interface(ip_address + '/' + subnet)
            if ipaddress.ip_address(ip_address).version != 4:
                # print('not ipv4')
                self.alert_queue.put(['Sorry I don\'t support modifying IPv6 yet.', 'No IPv6 support'])
                return False
            if ipaddress.ip_address(ip_address) in ipaddress.IPv4Network('0.0.0.0/24'):
                self.alert_queue.put(['This IP is in an invalid range.', 'Not supported by Windows'])
                return False
            # print('check dup')
            if subnet_change:
                # no need to check the IP address it hasn't changed
                return True
            dup = self.check_if_duplicate(ip_address)
            if dup:
                # self.error_list.append([ip_address, dup])
                # pass
                current_interface = self.interface_list[self.adapt_cmb.GetSelection()]
                if current_interface == dup:
                    message = (F'Sorry {ip_address} all ready exists on interface {dup.description}.')
                else:
                    message = (f'Sorry {ip_address} already exists on {dup.description}.\r' +
                               f'To set {ip_address} here you will need either set {dup.description} to DHCP or\r' +
                               f'remove {ip_address} from {dup.description}.')
                # dispatcher.send(signal="Error", sender=self, info=message, caption="Duplicate IP\'s")
                self.alert_queue.put([message, 'Duplicate IP\'s'])
                return False

        except Exception as error:
            # print('dialog saying invalid: ', error)
            self.alert_queue.put([str(error), 'Invalid IP address'])
            return False
        return True

    def cell_update_ip(self, line, ip_address):
        # print('in update ip: ', line, ip_address)
        if line.ip_address == ip_address:
            # No change
            return
        # line.ip_address = ip_address
        # check if ipv4 or ipv6 and if valid
        if ipaddress.ip_address(line.ip_address).version != 4:
            self.alert_queue.put(['Sorry I don\'t support modifying IPv6 yet.', 'No IPv6 support'])
            return
        if not self.verify_ip_valid(ip_address=ip_address,
                                    subnet=line.subnet):
            return

        # Clean up address by passing it through ipaddress module
        ip_address = str(ipaddress.ip_address(ip_address))
        # Find current adapter  and update and set
        adapter = self.get_current_adapter()
        try:
            mod_index = adapter.ip_addresses.index(line.ip_address)
            adapter.ip_addresses[mod_index] = ip_address
        except Exception as error:
            print('Cell Update IP error: ', error)
            adapter.ip_addresses.append(ip_address)
            adapter.subnet_addresses.append(line.subnet)
        # print('backup: ', self.adapter_backup)
        self.set_static()
        # self.check_for_change(ip=ip_address, subnet=line.subnet)

    def cell_update_subnet(self, line, subnet):
        # print('in update subnet: ', line, subnet)
        if line.subnet == subnet:
            # No change
            return
        # line.ip_address = subnet
        # check if ipv4 or ipv6 and if valid
        if ipaddress.ip_address(line.ip_address).version != 4:
            self.alert_queue.put(['Sorry I don\'t support modifying IPv6 yet.', 'No IPv6 support'])
            return
        if not self.verify_ip_valid(ip_address=line.ip_address,
                                    subnet=subnet,
                                    subnet_change=True):
            return

        # Find current adapter  and update and set
        adapter = self.get_current_adapter()
        try:
            mod_index = adapter.ip_addresses.index(line.ip_address)
            adapter.subnet_addresses[mod_index] = subnet
        except Exception as error:
            print('Cell Update Subnet error: ', error)
            adapter.ip_addresses_addresses.append(line.ip_address)
            adapter.subnet_addresses.append(subnet)
        # print('backup: ', self.adapter_backup)
        self.set_static()
        # self.check_for_change(ip=subnet, subnet=line.subnet)

    def on_clear_settings(self, event):
        self.preferences = Preferences()
        self.ip_list.DeleteAllItems()
        self.control_queue.put(['send_update'])

    def on_right_click(self, event):
        if self.dhcp_btn.GetValue():
            return
        num_selected = 0
        for line in event.GetEventObject().GetSelectedObjects():
            if ipaddress.ip_address(line.ip_address).version != 4:
                return
            else:
                num_selected += 1
        count = 0
        for item in self.ip_list.GetObjects():
            if ipaddress.ip_address(item.ip_address).version == 4:
                count += 1
        if count == num_selected:
            self.rc_on_del.Enable(False)
        else:
            self.rc_on_del.Enable(True)
        self.olv_panelOnContextMenu(event)

    def get_current_adapter(self):
        current_interface = self.interface_list[self.adapt_cmb.GetSelection()]
        for adapter in self.preferences.adapter_prefs:
            # print('com: ', adapter.index, current_interface.index)
            if adapter.index == current_interface.index:
                # print('adapter: ', adapter)
                return adapter

    def on_add(self, event):
        adapter = self.get_current_adapter()
        dia = adapter_prefs.AddIPDialog(self, adapter)
        dia.ShowModal()
        dia.Destroy()

    def on_delete(self, event):
        """Remove from adapter and set ips"""
        adapter = self.get_current_adapter()
        for item in self.ip_list.GetSelectedObjects():
            try:
                my_ip = adapter.ip_addresses.index(item.ip_address)
                adapter.ip_addresses.pop(my_ip)
                adapter.subnet_addresses.pop(my_ip)
            except Exception as error:
                print('unable to remove ip from adapters: ', error)

        self.ip_list.RemoveObjects(self.ip_list.GetSelectedObjects())

        self.set_static()

    def on_refresh(self, event):
        self.control_queue.put(['check_changes'])
        if self.preferences.monitor_serial:
            self.control_queue.put(['check_serial'])

    def update(self, sender, interface_list):
        """Processes updates"""
        self.interface_list = interface_list
        new_interface_list = interface_list[::]
        matched_list = []
        for interface in new_interface_list:
            # See if it already exists and create it if not
            for adapter in self.preferences.adapter_prefs:
                if adapter.index == interface.index:
                    # This adapter exists move on
                    matched_list.append(interface)
        # print(matched_list)
        for item in matched_list:
            new_interface_list.remove(item)
        # print(new_interface_list)
        for interface in new_interface_list:
            # if we get here we didn't find a match
            # we need to create a new adapter
            new_adapter = Adapter(index=interface.index,
                                  ip_addresses=interface.ip_addresses,
                                  subnet_addresses=interface.subnet_addresses,
                                  gateway_addresses=interface.gateway_addresses)
            self.preferences.adapter_prefs.append(new_adapter)
        self.save_config(self.preferences)
        self.update_combo()

    def serial_update(self, sender, serial_list):
        # print(serial_list)
        for item in serial_list:
            self.tray_icon.ShowBalloon(f'Found new serial port {item.device}',
                                       f'{item.description}\r{item.hwid}', 10)
        self.preferences.last_serial_ports = serial_list
        self.save_config(preferences=self.preferences)

    def alert(self, sender, caption, message):
        dlg = wx.MessageDialog(parent=self,
                               message=message,
                               caption=caption,
                               style=wx.OK)
        dlg.ShowModal()

    def update_combo(self):
        """Updates the combo box"""
        self.adapt_cmb.Clear()
        for interface in self.interface_list:
            self.adapt_cmb.AppendItems(interface.description)
        if len(self.adapt_cmb.GetItems()) > 0:
            try:
                last = self.adapt_cmb.GetItems().index(self.selected)
                self.adapt_cmb.SetSelection(last)
            except Exception:
                self.adapt_cmb.SetSelection(0)
            self.update_ip_list()
        else:
            self.ip_list.DeleteAllItems()
            self.adapt_cmb.SetValue('No active adapters')
            self.tray_icon.on_unknown()

    def on_dhcp_radio(self, event):
        # print("in on radio: ", event)
        if self.adapt_cmb.GetValue() == 'No active adapters':
            return
        # self.dhcp_btn.Enable(False)
        self.set_dhcp()

    def on_static_radio(self, event):
        # print("Static selected")
        if self.adapt_cmb.GetValue() == 'No active adapters':
            return
        # self.static_btn.Enable(False)
        self.set_static()

    def set_dhcp(self):
        self.control_queue.put(['set_dhcp', self.interface_list[self.adapt_cmb.GetSelection()]])
        self.ip_list.DeleteAllItems()

    def set_static(self):
        current_interface = self.interface_list[self.adapt_cmb.GetSelection()]
        for item in self.preferences.adapter_prefs:
            if item.index == current_interface.index:
                # We have an existing config
                # We just switch to static at this point
                # print(item)
                self.control_queue.put(['set_static', item])
                return

    def create_line(self):
        """For use from other modules"""
        return IPLine()

    def update_ip_list(self):
        self.ip_list.DeleteAllItems()
        current_interface = self.interface_list[self.adapt_cmb.GetSelection()]
        for ip in current_interface.ip_addresses:
            index = current_interface.ip_addresses.index(ip)
            self.ip_list.AddObject(IPLine(ip_address=ip, subnet=current_interface.subnet_addresses[index]))

        if current_interface.dhcp_enabled:
            self.dhcp_btn.SetValue(True)
            self.tray_icon.on_dhcp()
            self.ip_list.cellEditMode = ObjectListView.CELLEDIT_NONE

        else:
            self.static_btn.SetValue(True)
            self.static_btn.SetValue(True)
            self.tray_icon.on_static()
            self.ip_list.cellEditMode = ObjectListView.CELLEDIT_DOUBLECLICK

    def on_select_adapter(self, event):
        # print('in select adapter')
        self.selected = self.interface_list[self.adapt_cmb.GetSelection()].description
        self.update_ip_list()

    def on_check_for_updates(self, event):
        self.preferences.check_for_updates = self.updates_chk.IsChecked()
        self.save_config(preferences=self.preferences)
        if self.updates_chk.IsChecked():
            self.do_update_check()

    def on_exit(self, event):
        """Close out the program"""
        self.onClose(event)

    def onClose(self, event):
        """
        Destroy the taskbar icon and the frame
        """
        self.save_config(preferences=self.preferences)
        self.tray_icon.RemoveIcon()
        self.tray_icon.Destroy()
        self.Destroy()

    def onMinimize(self, event):
        """
        When minimizing, hide the frame so it "minimizes to tray"
        """
        self.Hide()
        # self.toggle_show_gui(event)

    def toggle_show_gui(self, event=None):
        """Show or hide the GUI"""
        # We need to check a widget
        if self.IsShown():
            # hide
            self.Hide()
        else:
            self.Show()
            self.Restore()
            self.ToggleWindowStyle(wx.STAY_ON_TOP)  # Turn off
            self.ToggleWindowStyle(wx.STAY_ON_TOP)  # Turn on -- now on top

    def bring_to_top(self, event=None):
        """Brings window to top"""
        if self.IsShown():
            self.ToggleWindowStyle(wx.STAY_ON_TOP)  # Turn off
            self.ToggleWindowStyle(wx.STAY_ON_TOP)  # Turn on -- now on top
        else:
            self.Show()     # stop hiding
            self.Restore()  # restore window
            self.Show()     # show window on top?

            # self.ToggleWindowStyle(wx.STAY_ON_TOP)  # Turn off
            # self.ToggleWindowStyle(wx.STAY_ON_TOP)  # Turn on -- now on top

    def load_config(self):
        """Load Config"""
        try:
            with open(self.storage_file, 'rb') as f:
                return pickle.load(f)
        except Exception as error:
            print('unable to load plk ', error)
            return Preferences()

    def save_config(self, preferences=None):
        """Update values in config file"""
        if preferences is None:
            # Create new config file
            preferences = Preferences()
        if not os.path.exists(self.storage_file):
            os.mkdir(self.doc_path)
        with open(self.storage_file, "wb") as f:
            # self.preferences.main_list = self.main_list.GetObjects()
            pickle.dump(preferences, f)


class App(wx.App):
    def __init__(self, redirect=False, filename=None):
        wx.App.__init__(self, redirect, filename)

    def OnInit(self):
        self.name = "WinIP-%s".format(wx.GetUserId())
        self.instance = wx.SingleInstanceChecker(self.name)
        if self.instance.IsAnotherRunning():
            wx.MessageBox("An instance of WinIP is already running\r" +
                          "Please check your system tray for the WinIP icon",
                          "Error",
                          wx.OK | wx.ICON_WARNING)
            return False
        self.frame = Win_IP_Frame(parent=None)
        self.SetTopWindow(self.frame)
        wx.GetApp().Bind(wx.EVT_QUERY_END_SESSION, self.OnEvtClose)
        return True

    def OnEvtClose(self, event):
        self.frame.onClose(event)


def main():
    """Launch the main program"""

    winip_interface = App(redirect=False, filename="log.txt")
    winip_interface.MainLoop()


if __name__ == '__main__':
    if not admin.isUserAdmin():
        admin.runAsAdmin()
    else:
        main()
